<?php
function init() {
   $bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'root', 'aqwXSZ21');
   return $bdd;
}

function insertion($bdd) {
 $statement = $bdd->prepare('INSERT INTO message(username, text, post_date) VALUES(:username, :text, :post_date)');
 $statement->execute([":username"=>$_POST['username'],":text"=>$_POST['message'],":post_date"=>date('y-M-d H:i:s')]);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//  Attention une commande a été ajouté :                                                                //
//  - "/clear" pour que la base de donnée soit effacé(utilisable uniquement si le username est admin).   //
//  De plus le /ban doit être exectué avec l'username admin.                                    a        //
///////////////////////////////////////////////////////////////////////////////////////////////////////////
function chopperLeMessage($bdd) {
    $reponse = $bdd->query('SELECT * FROM message');
    $tab = explode(' ', $_POST['message']); //j'ai cherché avec les substr mais c'est la galère alors Gautier m'a orienté vers les explode/implode :)
    if($tab[0]  === "/ban" && $_POST['username'] == 'admin'){ //on test le /ban et le nom d'utilisateur
            $_SESSION['username_banned'] = $tab[1]; //on stock dans session pour que ça passe de page ne page(je suis pas certain que se soit nécéssaire mais ça marche :) )
            $_SESSION['reason'] = implode(' ', array_slice($tab, 2)); //implode(...,array_slice(...)) pour reconstituer la raison du ban dans un seul string
        }
    if($tab[0] === '/clear' && $_POST['username'] == 'admin') { //pareil que le ban avec clear
        $reponse = $bdd->query('TRUNCATE TABLE message'); //le /clear fonctionne !
        return 0; //on sort de la fct car on affiche rien
    }
    if(($tab[0] === '/clear' || $tab[0] === '/ban') && $_POST['username'] !== 'admin') { //si le user n'est pas admin alors un message d'erreur apparait et un gif aussi :)
        echo 'Erreur vous n\'avez pas les droits pauvre fou ! <img src="tenor.gif"> ';
        return 0;
    }
    while ($data = $reponse->fetch()) { // on stock la requete dans un tableau data
        if($data['username']  === $_SESSION['username_banned']) { 
            echo 'Censuré par la modération parce que : '.$_SESSION['reason'].'<br>'; 
        }
        else {
            if(substr($data['text'], 0, 3)  === "/me") { //ici j'ai utilisé le substr pour changer :)
                echo '<i>'.$data['post_date'].' : '.$data['username'].' : '.substr($data['text'], 3).'</i><br>';
            } // la grosse condition en dessous c'est pour pas que ça affiche les commandes de ban et de clear ^^'
            else if($data['username']  !== $_SESSION['username_banned'] && substr($data['text'], 0, 4) !== "/ban" && substr($data['text'], 0, 6) !== "/clear") {
                echo $data['post_date'].' : '.$data['username'].' : '.$data['text'].'<br>'; 
            }
        }
    }   
}

