<?php
define('TARGET_DIRECTORY', './images/'); // on définit une constante qui pour le dossier où seront stocké les images
if(!empty($_FILES['photo'])) {
move_uploaded_file($_FILES['photo']['tmp_name'], TARGET_DIRECTORY . $_FILES['photo']['name']); // on déplace l'image du formulaire
}
?>

<!DOCTYPE html>
<html>
    
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width" />
    <link rel='stylesheet' href='style.css' type='text/css' />
    <title>L'épicerie de Jean-Michel</title>
  </head>

  <body>
    <form action="./" method="POST" enctype="multipart/form-data">
      <p><h2>Ajouter un article au magasin :</h2></p>
      <div id="lien_menu"> <p> <a href="./liste_produits.php">Vers le menu</a> </p> </div><br><br>
      <fieldset><br>
        <div id="div1">
            <label for="photo">Photo de l'article :<br><br></label>
              <?php echo "<img src='./images/".$_FILES['photo']['name'].$_FILES['photo']['extension']."'alt='Prévisualisation' width='100px' height='100px'/>"; ?><br><br>
              <input type="file" name="photo" id="photo" accept=".jpg,.jpeg,.png" required/>
            <br><br>
            <label for="nom">Nom de l'article :<br></label>
              <input type="text" name="nom" id="nom" placeholder="Nom" required/>
            <br><br>
            <label for="prix">Prix :<br></label>
              <input type="text" name="prix" id="prix" placeholder="Prix" required/>
              <br><br>
            <label for="stock">Nombre objets en stock :<br></label>
            <input type="number" min="0" name="stock" id="stock" placeholder="Entrez le nombre ici" required/>
            <br><br>
        </div>

      </fieldset>
      <br>
      <br>
      <input type="submit" id="sender" value="Envoyer" />
    </form>
  </body>
</html>
