<?php
function init() {
   $bdd = new PDO('mysql:host=localhost;dbname=event_calendar;charset=utf8', 'root', 'aqwXSZ21',
           [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
   return $bdd;
}

function redirect($errorMessage, $link){
    $_SESSION['error'] = $errorMessage;
    header("Location: ".$link."");
    echo '<div class="alert alert-info" role="alert">".$errorMessage."</div>';
}

function identifier($bdd, $password, $login){
    $loginFromBdd = $bdd->query('SELECT login FROM Users');
    $result = $loginFromBdd->fetchAll();
    foreach ($result as $value) {
        if($value['login'] == $login){
            $hashedPassword = $bdd->query('SELECT password FROM Users WHERE login = "'.$value['login'].'"');
            $hashedPassword = $hashedPassword->fetch();
            if(password_verify($password, $hashedPassword[0])){
                return 1;
            }
            else { return 0; }
        }
    }
    return 0;
}

function boxDisplay4Customer($bdd, $dayNumber, $monthNumber, $yearNumber){
    $currDate = date("Y-m-d", mktime(0, 0, 0, $monthNumber, $dayNumber, $yearNumber));
    $d1 = $currDate." 00:00:00";
    $d2 = $currDate." 23:59:59";
    $reponse = $bdd->query('SELECT * FROM events WHERE startDate BETWEEN "'.$d1.'" AND "'.$d2.'"');
    $reponse= $reponse->fetchAll();
    $n = 0;
    
    foreach ($reponse as $value){
        if($value['nb_place'] == 0){ //if the event is full display the link in red
            $finalString = $finalString.'<a class="btn btn-outline-danger btn-sm flex-row " href="./customerEvent.php?date='.$currDate.'">'.$value['name'].'- Full</a><br>';
        }
        else{
            $finalString = $finalString.'<a class="btn btn-outline-primary btn-sm flex-row " href="./customerEvent.php?date='.$currDate.'">'.$value['name'].'</a><br>';
        }
            $n++;
        if($n == 5){ // if there is more than 5 events then display a "see more" button and break out the function
            $finalString = $finalString.'<a class="btn btn-outline-primary btn-sm flex-row " href="./customerEvent.php?date='.$currDate.'">See more...</a><br>';
            return $finalString;
        }
    }
    
    return $finalString;
}

function boxDisplay4Organizer($bdd, $dayNumber, $monthNumber, $yearNumber){
    $currDate = date("Y-m-d", mktime(0, 0, 0, $monthNumber, $dayNumber, $yearNumber));
    $d1 = $currDate." 00:00:00";
    $d2 = $currDate." 23:59:59";
    $reponse = $bdd->query('SELECT * FROM events WHERE startDate BETWEEN "'.$d1.'" AND "'.$d2.'"');
    $reponse= $reponse->fetchAll();
    $n = 0;
    
    foreach ($reponse as $value){
        if($_SESSION['id'] == $value['organizer_id']){
            $finalString = $finalString.'<a class="btn btn-outline-primary btn-sm flex-row " href="./organizerEvent.php?date='.$currDate.'">'.$value['name'].'</a><br><br>';
            $n++;
            if($n == 5){ // if there is more than 5 events then display a "see more" button and break out the function
                $finalString = $finalString.'<a class="btn btn-outline-primary btn-sm flex-row " href="./customerEvent.php?date='.$currDate.'">See more...</a><br>';
                return $finalString;
            }
        }    
    }
    return $finalString;
}

function displayDaysNumber($bdd, $month, $year){
    $week = ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"]; 
    $firstDay = date("D", mktime(0, 0, 0, $month, 1, $year));
    $numberOfDay = cal_days_in_month(CAL_GREGORIAN, $month, $year); //usefull function who give me the number of day in the current month
    
    for($i = 0; $i < 7; $i++){ // here I research the number of empty cases before the month begin
        if($week[$i] == $firstDay){
            $numberOfFirstDay = $i;
        }
    }
    echo '<tr>';
    for($i = 0; $i < $numberOfFirstDay; $i++){ // here I print the empty cases
        echo '<td></td>';
    }
    $n = 1;
    for($i = $numberOfFirstDay; $i < ($numberOfDay + $numberOfFirstDay); $i++){
        if($_SESSION['isOrga'] == 1){
            echo '<td>'.$n.'<br>'. boxDisplay4Organizer($bdd, $n, $month, $year).'</td>'; //then I call the display functions
        }
        else{ echo '<td>'.$n.'<br>'.boxDisplay4Customer($bdd, $n, $month, $year).'</td>'; }
        $n++;
        if(($i + 1) % 7 == 0){
            echo '</tr><tr>';
        }
    }
    echo '</tr>';
}

function sendEvent2BDD($bdd){
     $statement = $bdd->prepare('INSERT INTO events(name, startDate, endDate, nb_place, description, organizer_id) VALUES(:name, :startDate, :endDate, :nb_place, :description, :organizer_id)');
     $statement->execute([":name"=>$_POST['name'],":startDate"=>$_POST['startDate'],":endDate"=>$_POST['endDate'],":nb_place"=>$_POST['places'],":description"=>$_POST['description'],":organizer_id"=>$_SESSION['id']]);
}

function displayEvent4Organizer($currDate, $bdd){
    $d1 = $currDate." 00:00:00";
    $d2 = $currDate." 23:59:59";
    $reponse = $bdd->query('SELECT * FROM events WHERE startDate BETWEEN "'.$d1.'" AND "'.$d2.'"');
    $reponse= $reponse->fetchAll();

    foreach ($reponse as $value){
        if($_SESSION['id'] == $value['organizer_id']){
            echo '<div class="border border-primary rounded"><br>';
            echo '<h4>'.$value['name'].'</h4>';
            echo 'Starting Date : '.$value['startdate'].'<br>Endding Date : '.$value['enddate'].'<br><br>';
            echo '<h5>Description</h5><br>'.$value['description'].'<br><br>';
            if($value['nb_place'] != 0){ 
                echo 'Only '.$value['nb_place'].' left<br><br>';
            }
            else {
                echo 'No more places left<br><br>';
            }
            // here I pick up the name of the cancelled event in the link in order to delete it when I call calendar.php
            echo '<a class="btn btn-danger btn-lg flex-row " href="./organizerEvent.php?date='.$currDate.'&&canceled_name='.$value['name'].'">Cancel the Event</a><br><br>';
            echo '</div><br><br>';
        }
    }
}

function displayEvent4Customer($currDate, $bdd){
    $d1 = $currDate." 00:00:00";
    $d2 = $currDate." 23:59:59";
    $reponse = $bdd->query('SELECT * FROM events WHERE startDate BETWEEN "'.$d1.'" AND "'.$d2.'"');
    $reponse= $reponse->fetchAll();

    foreach ($reponse as $value){
        if($value['nb_place'] != 0){
            echo '<div class="border border-primary rounded"><br>';
            echo '<h4>'.$value['name'].'</h4>';
            echo 'Starting Date : '.$value['startdate'].'<br>Endding Date : '.$value['enddate'].'<br><br>';
            echo '<h5>Description</h5><br>'.$value['description'].'<br><br>';
            echo 'Only '.$value['nb_place'].' left<br><br>';
            // here I pick up the name of the cancelled event in the link in order to decrement the number of places when I call calendar.php
            echo '<a class="btn btn-primary btn-lg flex-row " href="./customerEvent.php?date='.$currDate.'&&sub_name='.$value['name'].'">Submit to the Event !</a><br><br>';
            echo '</div><br><br>';
        }
    }
    
}
