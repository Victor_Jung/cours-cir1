<?php include'functions.php';
session_start();
$bdd = init();
?>
<html>
    <head>
        <title>Login page</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
            body {
                font-size: large;
                font-family : Arial;  
                text-align: center;
            }
            a {
                color: white;
                text-decoration: none;
            }
            a:hover {
                color: white;
                text-decoration: none;
            }
        </style>
    </head>
    <body>
        <br>
        <h1 class="display-4">Google Calendar</h1><br>
        <br>    
        <?php
            if(!isset($_SESSION['currDate'])){
                $_SESSION['currDate'] = time();
            }
            if(isset($_GET['isOrga'])){
                $_SESSION['isOrga'] = 1;
            }
            if(isset($_GET['id'])){
                $_SESSION['id'] = $_GET['id'];
            }
            if(isset($_GET['month'])){
                //Passing month in the link, 2629800 is the average number of sec in 1 month
                $_SESSION['currDate'] = time() + $_GET['month']*2629800;
            }
            echo '<h3>'.date("F Y", $_SESSION['currDate']).'</h3';
        ?>
        <br>
        <?php
            echo '<a class="btn btn-primary btn-lg flex-row " href="./calendar.php?month='.($_GET['month'] - 1).'">Previous<a/>';
            echo '<a class="btn btn-primary btn-lg flex-row " href="./calendar.php?month='.($_GET['month'] + 1).'">Next<a/>';
        ?>
        </ul>
        <br><br>
        <table class="table table-bordered table-dark">
            <thead>
                <tr>
                    <th>Mon</th>
                    <th>Tue</th>
                    <th>Wed</th>
                    <th>Thu</th>
                    <th>Fri</th>
                    <th>Sat</th>
                    <th>Sun</th>
                </tr>
            </thead>
            <?php
                displayDaysNumber($bdd, date("n", $_SESSION['currDate']), date("Y", $_SESSION['currDate']));
            ?>
        </table>
        <br>
        <?php
            if($_SESSION['isOrga'] == 1){ //Display the button "Create an Event" only if this is an organizer
               echo '<a class="btn btn-primary btn-lg flex-row " href="./eventCreator.php">Create an Event<a/>';
            }
        ?>
        <br>
        <p class="mt-5 mb-3 text-muted">&copy; by Jung Victor CIR1 2017-2018</p>
        <br><br>
    </body>
</html>
