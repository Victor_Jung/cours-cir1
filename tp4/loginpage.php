<html>
    <head>
        <title>Login page</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
            body {
                font-size: large;
                font-family : Arial;  
                text-align: center;
            }
        </style>
    </head>
    <body>
        <br>
        <?php
            $bdd = init();
            if(identifier($bdd, $_POST['password'], $_POST['login']) != 1){
                echo '<div class="alert alert-info" role="alert">Incorrect password !</div>';
            }
        ?>
        <br>
        <h1 class="display-4">Google Calendar</h1><br>
        <form class="form-signin" action="./" method="POST" enctype="multipart/form-data">
        <div>
            <label class="sr-only" for="login"><br></label>
            <input type="text" name="login" id="login" placeholder=" Login" required autofocus/>
            <br><br>
            <label class="sr-only" for="password"></label>
            <input type="password" name="password" id="password" placeholder=" Password"></input>
            <br><br>
            <button class="btn btn-primary btn-lg" type="submit">Log in</button>
            <br><br>
            <p class="mt-5 mb-3 text-muted">&copy; by Jung Victor CIR1 2017-2018</p>
            
        </div>
    </body>
</html>
