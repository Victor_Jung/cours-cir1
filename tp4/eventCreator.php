<?php
    include'functions.php';
    session_start();
    $bdd = init();
    if(isset($_POST['name'])) {
        sendEvent2BDD($bdd);
    }
?>
<html>
    <head>
        <title>Login page</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
            body {
                font-size: large;
                font-family : Arial;  
                text-align: center;
            }
        </style>
    </head>
    <body>
        <br><br>
        <h1 class="display-4">Create your event</h1><br>
        <form class="form-signin" action="/eventCreator.php" method="POST" enctype="multipart/form-data">
        <div>
            <h3>Name of Event</h3>
            <label class="sr-only" for="name"><br></label>
            <input type="text" name="name" id="name" placeholder=" Name" required autofocus/>
            <br><br>
            <h3>Start Date </h3>
            <label class="sr-only" for="startDate"><br></label>
                <?php
                    $currDate4Form = date("Y-m-d",time())."T".date("G:m",time());
                    echo '<input type="datetime-local" name="startDate" id="startDate" value="'.$currDate4Form.'" required autofocus/>';
                ?>
            <br><br>
            <h3>End Date </h3>
            <label class="sr-only" for="endDate"><br></label>
                <?php
                    echo '<input type="datetime-local" name="endDate" id="endDate" value="'.$currDate4Form.'" required autofocus/>';
                ?>
            <br><br>
            <h3>Number of places</h3>
            <label class="sr-only" for="places"><br></label>
            <input type="number" name="places" id="places" min="1" required autofocus/>
            <br><br>
            <h3>Description</h3>
            <label for="description"><br></label>
                <textarea name="description" id="description" rows="5" cols="35" placeholder=" Descrition of your Event"></textarea>
            <br><br>
            <button class="btn btn-primary btn-lg" type="submit">Send</button>
            <br><br>
            <p class="mt-5 mb-3 text-muted">&copy; by Jung Victor CIR1 2017-2018</p>
            
        </div>
    </body>
</html>

