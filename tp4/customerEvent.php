<?php
session_start();
include 'functions.php';
$bdd = init();

if(isset($_GET['sub_name'])){
    $newNumberOfPlaces = $bdd->query('SELECT * FROM events WHERE name="'.$_GET['sub_name'].'"');
    $newNumberOfPlaces = $newNumberOfPlaces->fetch();
    if($newNumberOfPlaces['nb_place'] > 0){
        $newNumberOfPlaces['nb_place'] = $newNumberOfPlaces['nb_place'] -1;
    }
    $bdd->query('UPDATE events SET nb_place = "'.$newNumberOfPlaces['nb_place'].'" WHERE name="'.$_GET['sub_name'].'"');
}
?>
<html>
    <head>
        <title>Login page</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
            body {
                font-size: large;
                font-family : Arial;  
                text-align: center;
            }
        </style>
    </head>
    <body>
        <h1 class="display-4 text-primary">Events</h1><br>
        <?php displayEvent4Customer($_GET['date'], $bdd); ?>
        <br><br>
        <a class="btn btn-primary btn-lg flex-row " href="calendar.php">Return to Calendar</a>
        <br><br>
    </body>
</html>


